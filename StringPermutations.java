package com.interview.coding.test;

import java.util.HashSet;
import java.util.Set;

public class StringPermutations {

	public static void main(String args[]) {
		findPossibleStrings(new HashSet<String>(), "", "abc").stream().forEach(System.out::println);
	}

	public static Set<String> findPossibleStrings(Set<String> resultSet, String possibleString, String currentString) {
		
		if (currentString.isEmpty()) {
			
			resultSet.add(possibleString + currentString);
		
		} else {
			
			for (int i = 0; i < currentString.length(); i++) {

				String newPossibility = possibleString + currentString.charAt(i);
				String devideCurrentString = currentString.substring(0, i)
						+ currentString.substring(i + 1, currentString.length());
				
				findPossibleStrings(resultSet, newPossibility, devideCurrentString);
			}
		}
		return resultSet;
	}

}
