package com.interview.coding.test;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

class ParenthesesGenerator {

	private static final String OPENING_BRACKET = "{";
	private static final String CLOSING_BRACKET = "}";

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no of pairs: ");
		Integer numberOfPais = sc.nextInt();
		sc.close();

		generate(numberOfPais).forEach(System.out::println);
	}

	public static List<String> generate(int noOfPairs) {

		List<String> validCombinations = new LinkedList<>();

		generateOpeningAndClosingPair(noOfPairs, noOfPairs, validCombinations, "");

		return validCombinations;
	}

	private static void generateOpeningAndClosingPair(int openingCount, int closingCount,
			List<String> validCombinations, String currentString) {

		if (openingCount == 0 && closingCount == 0) {
			validCombinations.add(currentString);
		} else {

			if (openingCount > 0) {

				generateOpeningAndClosingPair(openingCount - 1, closingCount, validCombinations,
						currentString + OPENING_BRACKET);
			}

			if (openingCount < closingCount) {

				generateOpeningAndClosingPair(openingCount, closingCount - 1, validCombinations,
						currentString + CLOSING_BRACKET);
			}
		}
	}

}
